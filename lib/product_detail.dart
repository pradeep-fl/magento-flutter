import 'dart:convert';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:magento_flutter/models/magento_user.dart';
import 'package:magento_flutter/models/productdetails.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

Future<ProductDetail> fetchProduct(String sku) async {
  final token = '9y9fm29t08gsfni6phmq1yecja3rqm5s';
  if (sku == null || sku == "null") {
    sku = "WP01-28-Black";
  }
  Map<String, String> headers = {
    HttpHeaders.contentTypeHeader: "application/json", // or whatever
    HttpHeaders.authorizationHeader: "Bearer $token",
  };
  final response = await http.get(
      'http://demo-acm-2.bird.eu/rest/all/V1/products/$sku',
      headers: headers);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return ProductDetail.fromJson(json.decode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}


Future<ProductDetail> addToCart(String sku,int qty) async {
  final token = '9y9fm29t08gsfni6phmq1yecja3rqm5s';
  if (sku == null || sku == "null") {
    sku = "WP01-28-Black";
  }
  
  Map<String, String> headers = {
    HttpHeaders.contentTypeHeader: "application/json", // or whatever
    HttpHeaders.authorizationHeader: "Bearer $token",
  };
  final response = await http.post(
      'http://demo-acm-2.bird.eu/rest/all/V1/products/$sku',
      body: {
  "cartItem": {
    "sku": sku,
    "qty": qty,
    "quote_id": "4"
  }
},
      headers: headers);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return ProductDetail.fromJson(json.decode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}



Future<String> fetchCart(String cusId) async {
  final token = '9y9fm29t08gsfni6phmq1yecja3rqm5s';

  Map<String, String> headers = {
    HttpHeaders.contentTypeHeader: "application/json", // or whatever
    HttpHeaders.authorizationHeader: "Bearer $token",
  };
  if(cusId != null){
  final response = await http.post(
      'http://demo-acm-2.bird.eu/rest/all/V1/customers/$cusId/carts',
      headers: headers);

  if (response.statusCode == 200) {
    String cartId = response.body;
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print(cartId+": cart");
    return cartId;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
  }else{
  final response = await http.post(
      'http://demo-acm-2.bird.eu/rest/all/V1/carts/',
      headers: headers);
  if (response.statusCode == 200) {
    String cartId = response.body;
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print(cartId+": cart");
    return cartId;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
  }

}

class Products extends StatefulWidget {
  final String sku;
  final MagentoUser user;

  const Products({Key key, this.sku,this.user}) : super(key: key);
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  Future<ProductDetail> futureProduct;
  Future<String> resCart;
  String cartId='';

  TextEditingController _controller = TextEditingController();

  @override
  Future<void> initState()  {
    super.initState();
    futureProduct = fetchProduct(widget.sku);
    if(widget.user != null){
    resCart =  fetchCart(widget.user.id).then((s) => cartId = s);
    print("CartId :"+cartId);
    }else{
      resCart =  fetchCart(null).then((s) => cartId = s);
      print("CartId :"+cartId);
    }
    
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text('Product Detail'),
      ),
      body: FutureBuilder<ProductDetail>(
          future: futureProduct,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Container(
                child: SafeArea(
                  top: false,
                  left: false,
                  right: false,
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 25,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              shape: BoxShape
                                  .rectangle, // BoxShape.circle or BoxShape.retangle
                              //color: const Color(0xFF66BB6A),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(4, 4),
                                  blurRadius: 10.0,
                                ),
                              ]),
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0),
                            ),
                            child: Image.network(
                                "http://demo-acm-2.bird.eu/media/catalog/product/cache/926507dc7f93631a094422215b778fe0" +
                                    snapshot.data.imageUrl,
                                // width: 300,
                                height: 250,
                                fit: BoxFit.fill),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15, right: 15),
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 25, bottom: 5),
                                  child: Text(
                                    snapshot.data.name,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 24,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 1),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              right: 10.0),
                                          child: Text(
                                            '\$' + snapshot.data.price,
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),

                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 20.0,
                                              bottom: 20.0,
                                              left: 250.0,
                                              right: 20.0),
                                          child: Center(
                                            child: Container(
                                              width: 60.0,
                                              foregroundDecoration:
                                                  BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(5.0),
                                                border: Border.all(
                                                  color: Colors.blueGrey,
                                                  width: 2.0,
                                                ),
                                              ),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child: TextFormField(
                                                      textAlign:
                                                          TextAlign.center,
                                                      decoration:
                                                          InputDecoration(
                                                            hintText: "1",
                                                        contentPadding:
                                                            EdgeInsets.all(8.0),
                                                        border:
                                                            OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      5.0),
                                                        ),
                                                      ),
                                                      controller: _controller,
                                                      keyboardType: TextInputType
                                                          .numberWithOptions(
                                                        decimal: false,
                                                        signed: true,
                                                      ),
                                                      inputFormatters: <
                                                          TextInputFormatter>[
                                                        WhitelistingTextInputFormatter
                                                            .digitsOnly
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    height: 38.0,
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            border: Border(
                                                              bottom:
                                                                  BorderSide(
                                                                width: 0.5,
                                                              ),
                                                            ),
                                                          ),
                                                          child: InkWell(
                                                            child: Icon(
                                                              Icons
                                                                  .arrow_drop_up,
                                                              size: 18.0,
                                                            ),
                                                            onTap: () {
                                                              int currentValue;
                                                              if (_controller
                                                                      .text
                                                                      .toString() ==
                                                                  "") {
                                                                currentValue =
                                                                    1;
                                                              } else {
                                                                currentValue =
                                                                    int.parse(
                                                                        _controller
                                                                            .text);
                                                              }

                                                              setState(() {
                                                                currentValue++;
                                                                _controller
                                                                        .text =
                                                                    (currentValue)
                                                                        .toString(); // incrementing value
                                                              });
                                                            },
                                                          ),
                                                        ),
                                                        InkWell(
                                                          child: Icon(
                                                            Icons
                                                                .arrow_drop_down,
                                                            size: 18.0,
                                                          ),
                                                          onTap: () {
                                                            int currentValue;
                                                            if (_controller.text
                                                                        .toString() ==
                                                                    "" ||
                                                                _controller.text
                                                                        .toString() ==
                                                                    "0") {
                                                              currentValue = 1;
                                                            } else {
                                                              currentValue =
                                                                  int.parse(
                                                                      _controller
                                                                          .text);
                                                            }

                                                            setState(() {
                                                              print(
                                                                  "Setting state");
                                                              currentValue--;
                                                              _controller.text =
                                                                  (currentValue >
                                                                              0
                                                                          ? currentValue
                                                                          : 0)
                                                                      .toString(); // decrementing value
                                                            });
                                                          },
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),

                                        // Text(
                                        //     '\$190',
                                        //     style: TextStyle(
                                        //       color: Colors.black,
                                        //         fontSize: 16,
                                        //         decoration: TextDecoration.lineThrough
                                        //     )
                                        // ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Column(
                                children: <Widget>[
                                  Container(
                                      alignment: Alignment(-1.0, -1.0),
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          'Description',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      )),
                                  Container(
                                      alignment: Alignment(-1.0, -1.0),
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. but also the leap into electronic typesetting Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16),
                                        ),
                                      )),
                                  SizedBox(
                                    height: 25,
                                  ),
                                  ButtonTheme(
                                      minWidth: 300.0,
                                      height: 50.0,
                                      child: RaisedButton(
                                        color: Colors.greenAccent,
                                        onPressed: () =>
                                            print("Button Pressed"),
                                        child: new Text("Add To Cart"),
                                      )),
                                  SizedBox(
                                    height: 25,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
            return Center(
              child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(
                      Colors.deepOrange[500])),
            );
          }),
    );
  }
}
