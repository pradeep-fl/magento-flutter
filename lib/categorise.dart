import 'dart:io';

import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:magento_flutter/models/category.dart';
import 'package:magento_flutter/models/magento_user.dart';
import 'package:magento_flutter/models/product.dart';
import 'package:magento_flutter/product_list.dart';
import 'package:magento_flutter/config.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


Future<Category> fetchCategory(String rootCat) async {
if(rootCat == null || rootCat == "null"){
  rootCat = "2";
}
Map<String, String> headers = {
  HttpHeaders.contentTypeHeader: "application/json", // or whatever
  HttpHeaders.authorizationHeader: "Bearer $TOKEN",
};
  final response = await http.get('$BASE_URL/rest/V1/categories?rootCategoryId=$rootCat&depth=2', headers: headers);


  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return Category.fromJson(json.decode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}





class Categorise extends StatefulWidget {
  final String rootCat;
  final MagentoUser user;

  Categorise({Key key, this.rootCat,this.user}) : super(key: key);

  
  @override
  _CategoriseState createState() => _CategoriseState();
}

class _CategoriseState extends State<Categorise> {
  Future<Category> futureCategory;

   @override
  void initState() {
    super.initState();
    futureCategory = fetchCategory(widget.rootCat);
  }

  final List<String> imgList = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "CATEGORIES",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: SafeArea(
          top: false,
          left: false,
          right: false,
          child: Container(
            child: FutureBuilder<Category>(
            future: futureCategory,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 8, left: 6, right: 6, bottom: 8),
              children: List.generate(snapshot.data.children_data.length, (index) {
                return Container(
                  child: Card(
                    clipBehavior: Clip.antiAlias,
                    child: InkWell(
                      onTap: () {
                        var route;
                       // print(snapshot.data.children_data[index].id.toString()+": id"+snapshot.data.children_data[index].product_count.toString()+": count"+snapshot.data.children_data[index].children_data.length.toString());
                        if((snapshot.data.children_data[index].children_data.length != 0)){
                        route = new MaterialPageRoute(
                          builder:(context) => new Categorise(rootCat: snapshot.data.children_data[index].id.toString()),
                          );
                        }else{
                         route = new MaterialPageRoute(
                          builder:(context) => new ProductList(catId: snapshot.data.children_data[index].id.toString(),user: widget.user,),
                          );
                        }
                       
                          Navigator.of(context).push(route);
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 180,
                            width: double.infinity,
                            child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              imageUrl: imgList[1],
                              placeholder: (context, url) => Center(
                                  child: CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(Colors.deepOrange[500])
                        )
                              ),
                              errorWidget: (context, url, error) => new Icon(Icons.error),
                            ),
                          ),
                          ListTile(
                              title: Text(
                                snapshot.data.children_data[index].name,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16
                                ),
                              )
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }),
            );
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // By default, show a loading spinner.
              return Center(
                        child: CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(Colors.deepOrange[500])
                        ),
                      );
            },
          ),
            
          )),
    );
  }
}
