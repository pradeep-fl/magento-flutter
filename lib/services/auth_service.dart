import 'dart:convert';
import 'dart:io';

import 'package:magento_flutter/config.dart';
import 'package:magento_flutter/models/magento_user.dart';
import 'package:magento_flutter/models/user.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';

class AuthService {
  final storage = FlutterSecureStorage();
  // Create storage
  Future<String> login(UserCredential userCredential) async {
    final response = await http.post(
      'http://demo-acm-2.bird.eu/rest/V1/integration/customer/token',
      body: {
        {
        'username': userCredential.usernameOrEmail,
        'password': userCredential.password
      }
      },
    );

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      // return User.fromJson(json.decode(response.body));
      setUser(response.body);
      return jsonDecode(response.body);
    } else {
      if (response.statusCode == 403) {
        Fluttertoast.showToast(
            msg: "Invalid Credentials",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            fontSize: 16.0);
      }
      // If that call was not successful, throw an error.
//      throw Exception(response.body);
      return jsonDecode(response.body);
    }
  }

  Future<MagentoUser> register(UserReq user) async {

    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json", // or whatever
      HttpHeaders.authorizationHeader: "Bearer $TOKEN",
    };

Map jsonData = {
           'customer': {
          'firstname': user.firstname,
          'lastname': user.lastname,
          'email': user.email
        },
        "password":user.password
        };
    final response = await http.post(
        '$BASE_URL/rest/V1/customers',
        body: jsonEncode(jsonData),
        headers: headers);
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      // return User.fromJson(json.decode(response.body));

      return MagentoUser.fromJson(jsonDecode(response.body));
    } else {
      if (response.statusCode == 400) {
        Map result = jsonDecode(response.body);
        Fluttertoast.showToast(
            msg: result['message'],
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            fontSize: 16.0);
      }
      // If that call was not successful, throw an error.
//      throw Exception(response.body);
      return null;
    }
  }

  setUser(String value) async {
    await storage.write(key: 'user', value: value);
  }

  getUser() async {
    String user = await storage.read(key: 'user');
    if (user != null) {
      return jsonDecode(user);
    }
  }

  logout() async {
    await storage.delete(key: 'user');
  }
}
