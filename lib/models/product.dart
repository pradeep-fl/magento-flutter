class Product{
  final String id;
  final String sku;
  final String name;
  final String attribute_set_id;
  final String price;
  final String status;
  final String visibility;
  final String type_id;
  final String created_at;
  final String updated_at;
  final List<Attr> custom_attributes;
  final String imageUrl;

  Product({this.id, this.sku, this.name, this.attribute_set_id, this.price, this.status, this.visibility, this.type_id, this.created_at, this.updated_at, this.custom_attributes,this.imageUrl});

factory Product.fromJson(Map<String, dynamic> json) {
   var attrJson = json['custom_attributes'] as List;
  return Product(
    id:json['id'].toString(),
    sku:json['sku'].toString(),
    name:json['name'], 
    attribute_set_id:json['attribute_set_id'].toString(), 
    price:json['price'].toString(), 
    status:json['status'].toString(), 
    visibility:json['visibility'].toString(), 
    type_id:json['type_id'].toString(), 
    created_at:json['created_at'].toString(), 
    updated_at:json['updated_at'].toString(),
    custom_attributes:attrJson.map((custom_attributes) => Attr.fromJson(custom_attributes)).toList(),
    );
}

}

class Attr{
  final String name;
  final String value;

  Attr({this.name, this.value});

factory Attr.fromJson(Map<String, dynamic> json){
  return Attr(
    name:json['attribute_code'].toString(),
    value:json['value'].toString()
  );
}
}