class Subcategory {
    final int id;
  final int parent_id;
  final String name;
  final bool is_active;
  final int position;
  final int level;
  final int product_count;
  List<Subcategory> children_data;

  Subcategory({this.id, this.parent_id, this.name,this.is_active,this.position,this.level,this.product_count,this.children_data});

  factory Subcategory.fromJson(Map<String, dynamic> json) {
    var subCatJson = json['children_data'] as List;

    return Subcategory(
      id: json['id'],
      parent_id: json['parent_id'],
      name: json['name'],
      is_active: json['is_active'],
      position: json['position'],
      level: json['level'],
      product_count: json['product_count'],
      children_data: subCatJson.map((children_data) => Subcategory.fromJson(children_data)).toList(),
    );
  }
}