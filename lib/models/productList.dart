import 'package:magento_flutter/models/product.dart';

class Product_List {
  final List<Product> products;

  Product_List({this.products});  

 factory Product_List.fromJson(Map<String, dynamic> json) {
   var itemsJson = json['items'] as List;
   return Product_List(
     products:itemsJson.map((products) => Product.fromJson(products)).toList()
   );
 }
}