class MagentoUser {
  final String id;
  final String group_id;
  final String created_at;
  final String updated_at;
  final String created_in;
  final String email;
  final String firstname;
  final String lastname;
  final String store_id;
  final String website_id;
  final String addresses;
  final String disable_auto_group_change;

  MagentoUser(
      {this.id,
      this.group_id,
      this.created_at,
      this.updated_at,
      this.created_in,
      this.email,
      this.firstname,
      this.lastname,
      this.store_id,
      this.website_id,
      this.addresses,
      this.disable_auto_group_change});

  factory MagentoUser.fromJson(Map<String, dynamic> json) {
    return MagentoUser(
        id: json['id'].toString(),
        group_id: json['group_id'].toString(),
        created_at: json['created_at'].toString(),
        created_in: json['created_in'].toString(),
        email: json['email'].toString(),
        firstname: json['firstname'].toString(),
        lastname: json['lastname'].toString(),
        store_id: json['store_id'].toString(),
        website_id: json['website_id'].toString(),
        addresses: json['addresses'].toString(),
        disable_auto_group_change: json['disable_auto_group_change'].toString());
  }
}
