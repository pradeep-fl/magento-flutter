class UserReq {
  String firstname;
  String lastname;
  String email;
  String password;
  UserReq({this.firstname,this.lastname, this.email, this.password});
  factory UserReq.fromJson(Map<String, dynamic> json) {
    return UserReq(
      firstname: json['firstname'],
      lastname:json['lastname'],
      email: json['email'],
      password: json['password']
    );
  }
}

class UserCredential {
  String usernameOrEmail;
  String password;
  UserCredential({this.usernameOrEmail, this.password});
}
