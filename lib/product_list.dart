import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:magento_flutter/models/magento_user.dart';
import 'package:magento_flutter/models/productList.dart';
import 'package:magento_flutter/product_detail.dart';
import 'package:magento_flutter/products_list_item.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:smooth_star_rating/smooth_star_rating.dart';

Future<Product_List> fetchProducts(String catId) async {
  final token = '9y9fm29t08gsfni6phmq1yecja3rqm5s';
  if (catId == null || catId == "null") {
    catId = "2";
  }
  Map<String, String> headers = {
    HttpHeaders.contentTypeHeader: "application/json", // or whatever
    HttpHeaders.authorizationHeader: "Bearer $token",
  };
  final response = await http.get(
      'http://demo-acm-2.bird.eu/rest/V1/products?searchCriteria[filterGroups][0][filters][0][field]=category_id& searchCriteria[filterGroups][0][filters][0][value]=$catId& searchCriteria[pageSize]=10& searchCriteria[currentPage]=1',
      headers: headers);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return Product_List.fromJson(json.decode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

class ProductList extends StatefulWidget {
  final String catId;
  final MagentoUser user;

  ProductList({Key key, this.catId,this.user}) : super(key: key);

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  Future<Product_List> futureProducts;

  @override
  void initState() {
    super.initState();
    futureProducts = fetchProducts(widget.catId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "PRODUCT LIST",
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: FutureBuilder<Product_List>(
            future: futureProducts,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Container(
                  child: GridView.count(
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    childAspectRatio: 0.7,
                    padding:
                        EdgeInsets.only(top: 8, left: 6, right: 6, bottom: 12),
                    children:
                        List.generate(snapshot.data.products.length, (index) {
                      return Container(
                        child: Card(
                          clipBehavior: Clip.antiAlias,
                          child: InkWell(
                            onTap: () {
                             var route = new MaterialPageRoute(
                          builder:(context) => new Products(sku: snapshot.data.products[index].sku.toString(),user: widget.user,),
                          );
                             Navigator.of(context).push(route);
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height:
                                      (MediaQuery.of(context).size.width / 2 -
                                          5),
                                  width: double.infinity,
                                  child: CachedNetworkImage(
                                    fit: BoxFit.cover,
                                    imageUrl:
                                        "https://images-na.ssl-images-amazon.com/images/I/71O0zS0DT0L._UX342_.jpg",
                                    //products[index]['image'],
                                    placeholder: (context, url) => Center(
                                        child: CircularProgressIndicator()),
                                    errorWidget: (context, url, error) =>
                                        new Icon(Icons.error),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 5.0),
                                  child: ListTile(
                                    title: Text(
                                      snapshot.data.products[index].name,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 11),
                                    ),
                                    subtitle: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 2.0, bottom: 1),
                                              child: Text(
                                                  "\$"+snapshot.data.products[index]
                                                      .price
                                                      .toString(),
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                    fontWeight: FontWeight.w700,
                                                  )),
                                            ),
                                            // Padding(
                                            //   padding: const EdgeInsets.only(
                                            //       left: 6.0),
                                            //   child: Text('(\$400)',
                                            //       style: TextStyle(
                                            //           fontWeight:
                                            //               FontWeight.w700,
                                            //           fontStyle:
                                            //               FontStyle.italic,
                                            //           color: Colors.grey,
                                            //           decoration: TextDecoration
                                            //               .lineThrough)),
                                            // )
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            SmoothStarRating(
                                                allowHalfRating: false,
                                                onRatingChanged: (v) {
                                                  //products[index]['rating'] = v;
                                                  setState(() {});
                                                },
                                                starCount: 5,
                                                rating: 3,
                                                //products[index]['rating'],
                                                size: 16.0,
                                                color: Colors.amber,
                                                borderColor: Colors.amber,
                                                spacing: 0.0),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 6.0),
                                              child: Text('(4)',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      color: Theme.of(context)
                                                          .primaryColor)),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
                  ),
                );
              }
              return Center(
                child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                        Colors.deepOrange[500])),
              );
            })
        //_buildProductsListPage(),
        );
  }

  _buildProductsListPage() {
    Size screenSize = MediaQuery.of(context).size;
    return Container(
        color: Colors.grey[100],
        child: FutureBuilder<Product_List>(
            future: futureProducts,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemCount: snapshot.data.products.length,
                  itemBuilder: (context, index) {
                    String imageURL = '';
                    if (index == 0) {
                      return _buildFilterWidgets(screenSize);
                    } else if (index == 4) {
                      return SizedBox(
                        height: 12.0,
                      );
                    } else {
                      for (var i = 0;
                          i <
                              snapshot.data.products[index].custom_attributes
                                  .length;
                          i++) {
                        if (snapshot.data.products[index].custom_attributes[i]
                                .name ==
                            "thumbnail") {
                          imageURL = snapshot
                              .data.products[index].custom_attributes[i].value;
                        }
                      }
                      return ProductsListItem(
                        name: snapshot.data.products[index].name,
                        currentPrice:
                            snapshot.data.products[index].price.toString(),
                        imageUrl:
                            //"http://demo-acm-2.bird.eu/media/catalog/product/cache/c687aa7517cf01e65c009f6943c2b1e9/$imageURL",
                            "https://images-na.ssl-images-amazon.com/images/I/71O0zS0DT0L._UX342_.jpg",
                      );
                      //return _dummyProductsList()[index];
                    }
                  },
                );
              }
              // By default, show a loading spinner.
              return Center(
                child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                        Colors.deepOrange[500])),
              );
            }));
  }

  _buildFilterWidgets(Size screenSize) {
    return Container(
      margin: const EdgeInsets.all(12.0),
      width: screenSize.width,
      child: Card(
        elevation: 4.0,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _buildFilterButton("SORT"),
              Container(
                color: Colors.black,
                width: 2.0,
                height: 24.0,
              ),
              _buildFilterButton("REFINE"),
            ],
          ),
        ),
      ),
    );
  }

  _buildFilterButton(String title) {
    return InkWell(
      onTap: () {
        print(title);
      },
      child: Row(
        children: <Widget>[
          Icon(
            Icons.arrow_drop_down,
            color: Colors.black,
          ),
          SizedBox(
            width: 2.0,
          ),
          Text(title),
        ],
      ),
    );
  }

  _dummyProductsList() {
    return [
      ProductsListItem(
        name: "Michael Kora",
        currentPrice: 524.toString(),
        imageUrl:
            "https://n1.sdlcdn.com/imgs/c/9/8/Lambency-Brown-Solid-Casual-Blazers-SDL781227769-1-1b660.jpg",
      ),
      ProductsListItem(
        name: "Michael Kora",
        currentPrice: 524.toString(),
        imageUrl:
            "https://n1.sdlcdn.com/imgs/c/9/8/Lambency-Brown-Solid-Casual-Blazers-SDL781227769-1-1b660.jpg",
      ),
      ProductsListItem(
        name: "David Klin",
        currentPrice: 249.toString(),
        imageUrl:
            "https://images-na.ssl-images-amazon.com/images/I/71O0zS0DT0L._UX342_.jpg",
      ),
      ProductsListItem(
        name: "Nakkana",
        currentPrice: 899.toString(),
        imageUrl:
            "https://assets.myntassets.com/h_240,q_90,w_180/v1/assets/images/1304671/2016/4/14/11460624898615-Hancock-Men-Shirts-8481460624898035-1_mini.jpg",
      ),
      ProductsListItem(
        name: "David Klin",
        currentPrice: 249.toString(),
        imageUrl:
            "https://images-na.ssl-images-amazon.com/images/I/71O0zS0DT0L._UX342_.jpg",
      ),
      ProductsListItem(
        name: "Nakkana",
        currentPrice: 899.toString(),
        imageUrl:
            "https://assets.myntassets.com/h_240,q_90,w_180/v1/assets/images/1304671/2016/4/14/11460624898615-Hancock-Men-Shirts-8481460624898035-1_mini.jpg",
      ),
    ];
  }
}
